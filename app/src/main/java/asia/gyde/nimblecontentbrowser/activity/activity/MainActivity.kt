package asia.gyde.nimblecontentbrowser.activity.activity

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import asia.gyde.nimblecontentbrowser.R

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}
